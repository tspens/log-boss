from pathlib import Path
import site

PROJECT_ROOT = str(Path(__file__).parent.absolute())
SITE_PACKAGES = site.getsitepackages()[0]
PTH_FILE = Path(f'{SITE_PACKAGES}/project.pth')

with PTH_FILE.open('w') as pth:
    pth.write(PROJECT_ROOT)
