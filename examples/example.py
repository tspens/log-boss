"""
Items to exercise:
>    * Threading
>    * Date formatting
>    * Default rules
>    * Disabling
>    * Masking
>    * Locking log tags
>    * Default and custom log tags
>    * Modes
>    * Redirect
>    * Settings rules
>    * Adding a cleanup
>    * Generating html pre and post op
>    * Logging exceptions
>    * Log wrappers
"""
from logboss.logger import Logger

logger = Logger()

for name in dir(logger):
    if not name.startswith('__'):
        print(name)
