from logboss import LogTagTypes, Logger
import os
import time
from contextlib import contextmanager

class ThreadingLogTags:
    def __init__(self):
        self.thread = LogTagTypes.Debug(
            name='New Thread'
        )

threading_log_tags = ThreadingLogTags()

class RulesLogTags:
    def __init__(self):
        self.rule_test = LogTagTypes.Info(
            name='Rule Test',
            value=1
        )

rules_log_tags = RulesLogTags()

logger = Logger()
logger.add_log_tags(threading_log_tags)
logger.add_log_tags(rules_log_tags)
log_file_stem = f'{os.path.dirname(__file__)}/logs-dev/log_{time.strftime("%Y%m%d%H%M%S")}'


@contextmanager
def start_logging(redirect=True):
    if redirect:
        logger.redirect = f'{log_file_stem}.txt'
    with logger.generate('html', log_file=f'{log_file_stem}.html', include_code=False):
        yield
