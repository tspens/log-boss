from os.path import dirname, join
from examples.logger import logger

json_path = join(f'{dirname(__file__)}/logs-dev', 'test_generators')

def cleanup():
    with logger.generate('html', log_file=f'{json_path}.html'):
        logger.load(json_file=f'{json_path}.json')

def run():
    logger.add_cleanup(cleanup)
    logger.start(True)
    logger.log('This should get picked up.')
    logger.dump(path=f'{json_path}.json')


if __name__ == '__main__':
    run()
