from examples.logger import logger, start_logging
from logboss import LogTagBase


@logger.wrap_class(
    log_tag='debug',
    mask_input_regexes=['VaR.*es'],
    mask_output_regexes=['asdf', 'stored'],
    func_regex_exclude='_.*|should_not_be_logged'
)
class LogFunctionTest:
    def __init__(self, x_input: int, y_input: int = 1):
        self._x = x_input
        self._y = y_input

    @classmethod
    def load(cls, variables: dict):
        return cls(**variables)

    @staticmethod
    def add(x: int, y: int):
        return x + y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @x.deleter
    def x(self):
        del self._x

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @y.deleter
    def y(self):
        del self._y

    def sum_it_up(self, expected_z_output: int):
        logger.debug(f'Summing up {self.x} + {self.y}...')
        self.load({'x_input': 1, 'y_input': 2})
        z = self.add(self.x, self.y)
        logger.debug(f'Result: {z}')
        return {
            'Actual Result': z,
            'Matched Expected': z == expected_z_output
        }

    def to_dict(self):
        return {
            'Stored': {
                'x': {
                    'type': 'int',
                    'value': self.x
                },
                'y': {
                    'type': 'int',
                    'value': self.y
                }
            },
            'Sum': self.x + self.y
        }

    @logger.wrap_func()
    def print_all(self):
        logger.info(f'X: {self.x}')
        logger.info(f'Y: {self.y}')

    def throw_exception(self):
        raise AssertionError('This exception is expected...')

    @staticmethod
    def should_not_be_logged():
        return 'Should not be logged!'

def run():
    lft = LogFunctionTest(x_input=1, y_input=2)
    lft2 = LogFunctionTest.load(dict(x_input=1, y_input=2))
    lft.print_all()
    lft2.to_dict()
    lft.sum_it_up(3)
    lft.throw_exception()


if __name__ == '__main__':
    logger.date_format = '%a %b %-d %-I:%M:%S %p'
    class DummyLogTags:
        def __init__(self):
            self.dummy_tag = LogTagBase(name='Dummy', value=999)
    with start_logging(redirect=False):
        try:
            logger.add_log_tags(DummyLogTags())
            logger.critical('Unexpected!')
        except:
            logger.info('Expected...')
        logger.info('This is an expected module-level logging message...')
        run()
