from examples.logger import logger


def run():
    logger.start()
    logger.log(log_tag='debug', msg='This is a debug message.')
    logger.log(log_tag='info', msg='This is an info message.')
    logger.log(log_tag='warning', msg='This is a warning message.')
    logger.log(log_tag='error', msg='This is an error message.')
    logger.log(log_tag='critical', msg='This is a critical message.')


if __name__ == '__main__':
    run()
