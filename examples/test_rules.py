from examples.logger import logger, start_logging, rules_log_tags
from logboss import LogTagBase


def min_tag_value_rule(tag: LogTagBase):
    if tag.value == logger.default_log_tags.critical.value:
        return True, True
    return False, False


def blacklisted_log_tag_names(tag: LogTagBase):
    if tag in (logger.default_log_tags.error, logger.default_log_tags.critical):
        return True, True
    return False, False


def run():
    logger.default_rule = lambda x: False
    logger.info('Testing default rule...')

    with logger.rule(min_level='critical', why='Testing min_level option...'):
        logger.debug('Unexpected!!')
        logger.info('Unexpected!!')
        logger.warning('Unexpected!!')
        logger.error('Unexpected!!')
        logger.critical('Expected...')

    with logger.rule(blacklist_function=lambda x: (True, False), why='Testing no console rule...'):
        logger.set_mode(mode='console')
        logger.critical('Unexpected!!')
        logger.set_mode(mode='persistence')
        logger.info('Expected...')

    with logger.rule(blacklist_function=lambda x: (False, True), why='Testing no persist rule...'):
        logger.set_mode(mode='console')
        logger.info('Expected...')
        logger.set_mode(mode='persistence')
        logger.critical('Unexpected!!')

    logger.set_mode(mode='all')

    logger.set_rule(blacklist_function=min_tag_value_rule, why='Testing min tag value rule...')
    logger.info('Expected...')
    logger.critical('Unexpected!!')
    logger.log('Expected...', log_tag=rules_log_tags.rule_test)

    log_rule = logger.rule(blacklist_function=lambda x: x is logger.default_log_tags.critical, why='Testing blacklisted names rule...')
    with log_rule:
        logger.info('Expected...')
        logger.critical('Unexpected!!')
        logger.log('Expected...', log_tag=rules_log_tags.rule_test)

    logger.set_rule(reset=True)
    logger.info('Expected...')
    with logger.disabled():
        logger.critical('Unexpected!!')
    logger.info('Expected...')

    with logger.rule(min_level='critical'):
        logger.info('You should not see a reset log.')


if __name__ == '__main__':
    with start_logging(redirect=False):
        run()
