from examples.logger import logger, threading_log_tags, start_logging
from threading import Thread


@logger.wrap_func(
    log_tag=threading_log_tags.thread,
    mask_input_regexes=['z.*tPU.'],
    mask_output_regexes=['Output', 'in..t']
)
def thread_test(x_input: int, y_input: int, z_output: int):
    logger.info(f'Testing {x_input} + {y_input} = {z_output}...')
    return {'Result': x_input + y_input == z_output, 'Inputs': [x_input, y_input], 'Output': z_output }


@logger.wrap_func()
def recursion_test(x: int = 10):
    logger.info(f'Recursion level {x}...')
    if x > 0:
        recursion_test(x-1)


def run():
    threads = [Thread(target=thread_test, args=(i, i + 1), kwargs={'z_output': i + 2})
               for i in range(10)]
    [t.start() for t in threads]
    recursion_test()
    [t.join() for t in threads]


if __name__ == '__main__':
    with start_logging():
        run()
